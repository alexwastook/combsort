import random as r
from timeit import timeit

toSort = [(0*i)+r.randrange(9999) for i in range(10**7)]

def quicksort(toSort):
	
	size = len(toSort)
	if size <= 1:
		return toSort

	left,middle,right = [],[],[]
	x = toSort[size//2]
	for i in toSort:
		if i<x:
			left.append(i)
		elif i>x:
			right.append(i)
		else:
			middle.append(i)

	return quicksort(left)+middle+quicksort(right)

print(timeit(lambda : quicksort(toSort)))

def combsort(toSort):
	size = len(toSort)
	gap = size-1
	while gap>0:
		for head in range(gap,size,1):
			tail = head-gap
			if(toSort[head]<toSort[tail]):
				tmp = toSort[head]
				toSort[head] = toSort[tail]
				toSort[tail] = tmp
		gap-=1
	return toSort

print(timeit(lambda : combsort(toSort)))